<?php
	$accao = 'consultarPendentes';
	require 'tarefaController.php';

?>

<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>App Lista Tarefas</title>

		<link rel="stylesheet" href="css/estilo.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	
		<script>
			function editarTarefa(idTarefa, descTarefa){
				//criar um form
				let form = document.createElement('form');
				form.action = 'tarefaController.php?pag=index&accao=editar';
				form.method = 'post';

				let rowEl = document.createElement('div');
				rowEl.className = 'row';

				let colmd8 = document.createElement('div');
				colmd8.className = 'col-md-8';

				let colmd4 = document.createElement('div');
				colmd4.className = 'col-md-4';

				//criar um input
				let inputTarefa = document.createElement('input');
				inputTarefa.type = 'text';
				inputTarefa.name = 'tarefa';
				inputTarefa.className = 'form-control';
				inputTarefa.value = descTarefa;

				let id = document.createElement('input');
				id.type = 'hidden';
				id.name = 'id';
				id.value = idTarefa;
				
				//criar um button
				let submitTarefa = document.createElement('button');
				submitTarefa.type = 'submit';
				submitTarefa.className = 'btn btn-info';
				submitTarefa.innerHTML = 'Concluir';

				

				rowEl.appendChild(colmd8);
				rowEl.appendChild(colmd4);

				colmd8.appendChild(inputTarefa);
				colmd8.appendChild(id);
				colmd4.appendChild(submitTarefa);

				form.appendChild(rowEl);

				//recuperar o id da tarefa
				idTarefa = 'descTarefa' + idTarefa;
				let tarefa = document.getElementById(idTarefa);
				//limpar conteudo interno
				tarefa.innerHTML = '';
				tarefa.insertBefore(form, tarefa[0]);

			}

			function removerTarefa(idTarefa){
				
				window.location.href="index.php?pag=index&accao=eliminar&id="+idTarefa;
			}

			function concluirTarefa(idTarefa){
				console.log("concluida");
				window.location.href="index.php?pag=index&accao=concluir&id="+idTarefa;
			}
		</script>

	</head>

	<body>
		<nav class="navbar navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">
					<img src="img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
					App Lista Tarefas
				</a>
			</div>
		</nav>

		<div class="container app">
			<div class="row">
				<div class="col-md-3 menu">
					<ul class="list-group">
						<li class="list-group-item active"><a href="#">Tarefas pendentes</a></li>
						<li class="list-group-item"><a href="nova_tarefa.php">Nova tarefa</a></li>
						<li class="list-group-item"><a href="todas_tarefas.php">Todas tarefas</a></li>
					</ul>
				</div>

				<div class="col-sm-9">
					<div class="container pagina">
						<div class="row">


							<div class="col">

	
								<h4>Tarefas pendentes</h4>
								<hr />
								
								<?php
									foreach($tarefas as $tarefa){
								?>

					

									<div class="row mb-3 d-flex align-items-center tarefa">
										<div class="col-sm-9" id="descTarefa<?= $tarefa->id ?>"><?= $tarefa->tarefa;?></div>
										<div class="col-sm-3 mt-2 d-flex justify-content-between">
		
											<i class="fas fa-trash-alt fa-lg text-danger" onclick="removerTarefa(<?= $tarefa->id ?>)"></i>
											<i class="fas fa-edit fa-lg text-info" onclick="editarTarefa(<?= $tarefa->id ?>, '<?= $tarefa->tarefa ?>')"></i>
											<i class="fas fa-check-square fa-lg text-success" onclick="concluirTarefa(<?= $tarefa->id ?>)"></i>
		
											
										</div>
									</div>
									
					

								<?php
									}
								?>
	
						
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>