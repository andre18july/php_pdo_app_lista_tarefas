Importante
Para testar o app, adicione a pasta app_lista_tarefas_public dentro do diretório público do lampp/xampp/wampp (htdocs) e a pasta app_lista_tarefas_private dois níveis acima (na pasta do lampp/xampp/wampp)

Queries necessárias
CREATE DATABASE php_pdo;

create table tb_usuarios( id int not null primary key auto_increment, nome varchar(50) not null, email varchar(100) not null, senha varchar(32) not null );

create table tb_status( id int not null primary key auto_increment, status varchar(25) not null );

insert into tb_status(status)values('pendente'); insert into tb_status(status)values('realizado');

create table tb_tarefas( id int not null primary key auto_increment, id_status int not null default 1, foreign key(id_status) references tb_status(id), tarefa text not null, data_cadastrado datetime not null default current_timestamp )