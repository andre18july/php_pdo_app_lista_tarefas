<?php   
    require_once('conexao.php');
    require_once('tarefa.model.php');
    require_once('tarefa.service.php');
    //require_once('../../../php_pdo_app_lista_tarefas_private/tarefa.service.php');

    /*
    echo '<pre>';
    print_r($_POST);    
    echo '</pre>'; 

    echo '<hr>';
    */


    $conexao = new Conexao();

    /*
    echo '<pre>';
    print_r($conexao);    
    echo '</pre>'; 

    echo '<hr>';
    */


    $accao = isset($_GET['accao']) ? $_GET['accao'] : $accao;


    if($accao== 'inserir')
    { 
        $tarefa = new Tarefa();
        $tarefa->__set('tarefa', $_POST['tarefa']);

        echo '<pre>';
        print_r($tarefa);    
        echo '</pre>'; 

        echo '<hr>';

        $tarefaService = new TarefaService($conexao, $tarefa);
        
        echo '<pre>';
        print_r($tarefaService);    
        echo '</pre>';

        $tarefaService->inserir();

        header('Location: nova_tarefa.php?inclusao=1');
    }

    else if($accao == 'consultar')
    {
        //echo "Consultar Todas";
        //obter as tarefas todas na bd

        $tarefa = new Tarefa();
        $tarefaService = new TarefaService($conexao, $tarefa);

        $tarefas = $tarefaService->obter();
    
    }

    else if($accao == 'editar')
    {
        echo 'Editar tarefa';
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';

        $tarefa = new Tarefa();
        $tarefa->__set('id', $_POST['id']);
        $tarefa->__set('tarefa', $_POST['tarefa']);

        echo '<pre>';
        print_r($tarefa);
        echo '</pre>';

        $tarefaService = new TarefaService($conexao, $tarefa);
        

        if($tarefaService->editar()){
            if(isset($_GET['pag']) && $_GET['pag'] == 'index'){
                header('Location: index.php');
            }else{
                header('Location: todas_tarefas.php');
            }
            
        }
    
    }

    else if($accao == 'eliminar')
    {
        echo '<pre>';
        print_r($_GET);
        echo '</pre>';
        $tarefa = new Tarefa();
        $tarefa->__set('id', $_GET['id']);

        $tarefaService = new TarefaService($conexao, $tarefa);

        if($tarefaService->eliminar()){
            if(isset($_GET['pag']) && $_GET['pag'] == 'index'){
                header('Location: index.php');
            }else{
                header('Location: todas_tarefas.php');
            }
        }
   
    }

    else if($accao == 'concluir')
    {
        echo '<pre>';
        print_r($_GET);
        echo '</pre>';
        $tarefa = new Tarefa();
        $tarefa->__set('id', $_GET['id']);
        $tarefa->__set('id_status', 2);

        $tarefaService = new TarefaService($conexao, $tarefa);

        if($tarefaService->concluir()){
            if(isset($_GET['pag']) && $_GET['pag'] == 'index'){
                header('Location: index.php');
            }else{
                header('Location: todas_tarefas.php');
            }
        }
   
    }

    else if($accao == 'consultarPendentes'){


        $tarefa = new Tarefa();
        $tarefa->__set('id_status', 1);

        $tarefaService = new TarefaService($conexao, $tarefa);

        $tarefas = $tarefaService->consultarPendentes();

    }
  



?>