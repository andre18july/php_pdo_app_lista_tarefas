<?php

    class TarefaService{

        
        private $conexao;
        private $tarefa;

        public function __construct(Conexao $conexao, Tarefa $tarefa){
            $this->conexao = $conexao->conectar();
            $this->tarefa = $tarefa;
        }

        public function inserir(){ //create
            $query = '
                insert into tb_tarefas(tarefa)
                values(:tarefa);
            ';
            
            try{
                $stmt = $this->conexao->prepare($query);
                $stmt->bindValue(':tarefa', $this->tarefa->__get('tarefa'));
                $stmt->execute();
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }
        }

        public function obter(){ //read
            $query = '
                select 
                    t.id, s.status, t.tarefa 
                from 
                    tb_tarefas as t
                    left join tb_status as s
                    on (t.id_status = s.id);
            ';

            try{
                $stmt = $this->conexao->prepare($query);
                $stmt->execute();
                $tarefas = $stmt->fetchAll(PDO::FETCH_OBJ);
                return $tarefas;
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }

        }

        public function editar(){ //update

            $query = '
                update
                    tb_tarefas
                set 
                    tarefa = ?
                where
                    id = ?
            ';

            try{
                $stmt = $this->conexao->prepare($query);
                $stmt->bindValue(1, $this->tarefa->__get('tarefa'));
                $stmt->bindValue(2, $this->tarefa->__get('id'));
                return $stmt->execute();
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }

            
        }

        public function eliminar(){ //delete
            $query = '
                delete
                    from tb_tarefas
                where
                    id = ?
            ';

            try{
                $stmt = $this->conexao->prepare($query);
                $stmt -> bindValue(1, $this->tarefa->__get('id'));
                return $stmt -> execute();
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }
        }


        public function concluir(){ //delete
            $query = '
                update
                    tb_tarefas
                set 
                    id_status = ?
                where
                    id = ?
            ';

            try{
                $stmt = $this->conexao->prepare($query);
                $stmt -> bindValue(1, $this->tarefa->__get('id_status'));
                $stmt -> bindValue(2, $this->tarefa->__get('id'));
                return $stmt -> execute();
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }
        }

        public function consultarPendentes(){ //read


            $query = '
                select 
                    t.id, s.status, t.tarefa 
                from 
                    tb_tarefas as t
                    left join tb_status as s
                    on (t.id_status = s.id)
                where 
                    t.id_status = :id_status
            ';


            try{
                $stmt = $this->conexao->prepare($query);
                $stmt->bindValue(':id_status', $this->tarefa->__get('id_status'));
                $stmt->execute();
                $tarefas = $stmt->fetchAll(PDO::FETCH_OBJ);
                return $tarefas;
            }catch(Exception $e){
                echo "Erro na ligação à BD";
                header('Location: todas_tarefas.php');
            }

        }

        
    }

?>